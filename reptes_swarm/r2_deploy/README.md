# Repte 2 Deploy

## Creem compose.yml, el fitxer està adalt

## Iniciem swarm

- docker swarm init

## Desplegar stack
```
docker stack deploy -c docker-compose.yml  myapp

Creating network myapp_webnet
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer
Creating service myapp_web
```
```
docker stack services myapp

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
kvnph6y0c4m9   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
zja38aykepjj   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
hg887co0edt3   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
ndkwkk6t2jxv   myapp_web          replicated   1/2        tcasanovas/comptador:latest       *:80->80/tcp
```
```
docker stack ps myapp

ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE                    ERROR                       PORTS
rdu97urv9b7m   myapp_portainer.1    portainer/portainer:latest        i18       Running         Running 18 seconds ago                                       
yldliyoq9fur   myapp_redis.1        redis:latest                      i18       Running         Running 20 seconds ago                                       
pmhwwikiwylc   myapp_visualizer.1   dockersamples/visualizer:stable   i18       Running         Running 19 seconds ago                                       
nzpwlqtqq3tb   myapp_web.1          tcasanovas/comptador:latest       i18       Running         Running less than a second ago                               
v7d552jfhhse    \_ myapp_web.1      tcasanovas/comptador:latest       i18       Shutdown        Failed 6 seconds ago             "task: non-zero exit (1)"   
g4j0djgpovkl   myapp_web.2          tcasanovas/comptador:latest       i18       Running         Running less than a second ago                               
0f1u3fzefooo    \_ myapp_web.2      tcasanovas/comptador:latest       i18       Shutdown        Failed 7 seconds ago             "task: non-zero exit (1)"   
```

- Esborrem el stack
```
docker stack rm myapp

Removing service myapp_portainer
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```
## Modificar el deploy

- Canviem el compose.yml, en el valor "replicas" en canvi de "2" el canviem a "3"

```
docker stack deploy -c compose.yml myapp

Creating network myapp_webnet
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer
```

```
docker stack services myapp

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
av0611wdbfzx   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
bclrj76arg4n   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
m9pp7hb7yppp   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
rcq0or9vrmcm   myapp_web          replicated   3/3        tcasanovas/comptador:latest       *:80->80/tcp
```

## Desplegar 5 rèpliques del servei web en comptes de 3

- Editem una altre vegada el compose.yml, canviant el valor de rèpliques a 5.

```
docker stack deploy -c compose.yml myapp

Updating service myapp_web (id: rcq0or9vrmcmmbqi0za6c2hs2)
image tcasanovas/comptador:latest could not be accessed on a registry to record
its digest. Each node will access tcasanovas/comptador:latest independently,
possibly leading to different nodes running different
versions of the image.

Updating service myapp_redis (id: bclrj76arg4n6yelsf29m9tuu)
Updating service myapp_visualizer (id: m9pp7hb7ypppy1zyxpucnlrri)
Updating service myapp_portainer (id: av0611wdbfzx1fllwke18jrwh)
```
```
docker stack services myapp

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
av0611wdbfzx   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
bclrj76arg4n   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
m9pp7hb7yppp   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
rcq0or9vrmcm   myapp_web          replicated   3/5        tcasanovas/comptador:latest       *:80->80/tcp
```

## Desplegar 2 rèpliques en comptes de 5

- Modifiquem compose.yml amb el valor repliques=2

```
docker stack deploy -c compose.yml myapp

Updating service myapp_portainer (id: av0611wdbfzx1fllwke18jrwh)
Updating service myapp_web (id: rcq0or9vrmcmmbqi0za6c2hs2)
image tcasanovas/comptador:latest could not be accessed on a registry to record
its digest. Each node will access tcasanovas/comptador:latest independently,
possibly leading to different nodes running different
versions of the image.

Updating service myapp_redis (id: bclrj76arg4n6yelsf29m9tuu)
Updating service myapp_visualizer (id: m9pp7hb7ypppy1zyxpucnlrri)

docker stack services myapp

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
av0611wdbfzx   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
bclrj76arg4n   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
m9pp7hb7yppp   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
rcq0or9vrmcm   myapp_web          replicated   2/2        tcasanovas/comptador:latest       *:80->80/tcp
```

## Docker service

- Diverses ordres de docker service per consultar i gestionar els serveis, tant si s'ha desplegat amb docker-compose com amb docker swarm

```
docker service ls

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
av0611wdbfzx   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
bclrj76arg4n   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
m9pp7hb7yppp   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
rcq0or9vrmcm   myapp_web          replicated   2/2        tcasanovas/comptador:latest       *:80->80/tcp

```
```
docker service ps myapp_web

ID             NAME              IMAGE                         NODE      DESIRED STATE   CURRENT STATE                     ERROR                       PORTS
1xnki79xojqq   myapp_web.1       tcasanovas/comptador:latest   i18       Running         Starting less than a second ago                               
5jb9afi5u4bd    \_ myapp_web.1   tcasanovas/comptador:latest   i18       Shutdown        Failed 5 seconds ago              "task: non-zero exit (1)"   
akuxulebatnq    \_ myapp_web.1   tcasanovas/comptador:latest   i18       Shutdown        Failed 19 seconds ago             "task: non-zero exit (1)"   
6omxpqqv02gs   myapp_web.2       tcasanovas/comptador:latest   i18       Running         Starting less than a second ago                               
yp5mt1nw3mb3    \_ myapp_web.2   tcasanovas/comptador:latest   i18       Shutdown        Failed 5 seconds ago              "task: non-zero exit (1)"   
fp3lgux3r8mx    \_ myapp_web.2   tcasanovas/comptador:latest   i18       Shutdown        Failed 19 seconds ago             "task: non-zero exit (1)"   

docker service ps myapp_visualizer

ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
hpqkzzt79ga7   myapp_visualizer.1   dockersamples/visualizer:stable   i18       Running         Running about a minute ago             

```
```
docker service logs myapp_redis

myapp_redis.1.y0n11ku9j2ej@i18    | 1:C 27 Nov 2023 11:05:10.899 # WARNING Memory overcommit must be enabled! Without it, a background save or replication may fail under low memory condition. Being disabled, it can also cause failures without low memory condition, see https://github.com/jemalloc/jemalloc/issues/1328. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
myapp_redis.1.y0n11ku9j2ej@i18    | 1:C 27 Nov 2023 11:05:10.899 * oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
myapp_redis.1.y0n11ku9j2ej@i18    | 1:C 27 Nov 2023 11:05:10.899 * Redis version=7.2.3, bits=64, commit=00000000, modified=0, pid=1, just started
myapp_redis.1.y0n11ku9j2ej@i18    | 1:C 27 Nov 2023 11:05:10.899 * Configuration loaded
myapp_redis.1.y0n11ku9j2ej@i18    | 1:M 27 Nov 2023 11:05:10.899 * monotonic clock: POSIX clock_gettime
myapp_redis.1.y0n11ku9j2ej@i18    | 1:M 27 Nov 2023 11:05:10.899 * Running mode=standalone, port=6379.
myapp_redis.1.y0n11ku9j2ej@i18    | 1:M 27 Nov 2023 11:05:10.899 * Server initialized
myapp_redis.1.y0n11ku9j2ej@i18    | 1:M 27 Nov 2023 11:05:10.902 * Creating AOF base file appendonly.aof.1.base.rdb on server start
myapp_redis.1.y0n11ku9j2ej@i18    | 1:M 27 Nov 2023 11:05:10.908 * Creating AOF incr file appendonly.aof.1.incr.aof on server start
myapp_redis.1.y0n11ku9j2ej@i18    | 1:M 27 Nov 2023 11:05:10.908 * Ready to accept connections tcp
```
```
docker service rm myapp_portainer

myapp_portainer

docker service ls

ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
tu54y39jcl4k   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
m8wtxp5bqwc1   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
hffe61h4c4pn   myapp_web          replicated   1/2        tcasanovas/comptador:latest       *:80->80/tcp
```
## Escalar serveis / SCALE services

```
docker service scale myapp_web=4

myapp_web scaled to 4
overall progress: 4 out of 4 tasks 
1/4: running   [==================================================>] 
2/4: running   [==================================================>] 
3/4: running   [==================================================>] 
4/4: running   [==================================================>] 
verify: Service converged 

docker service scale myapp_web=2

myapp_web scaled to 2
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 

docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
8gom2eqvy06r   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
99mgj22aps7i   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
u425mdxssuu0   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
6aaltxtuc5lf   myapp_web          replicated   2/2        tcasanovas/comptador:latest       *:80->80/tcp

docker service ps myapp_web

ID             NAME          IMAGE                         NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
bzdw5ox5x61m   myapp_web.1   tcasanovas/comptador:latest   i18       Running         Running 2 minutes ago             
gx425lm9o9y7   myapp_web.2   tcasanovas/comptador:latest   i18       Running         Running 2 minutes ago             

```
- Esborrem myapp amb docker stack
```
docker stack rm myapp

Removing service myapp_portainer
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

