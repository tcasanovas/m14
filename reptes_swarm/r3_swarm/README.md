## Repte 3 Docker SWARM

```
$ docker swarm init
Swarm initialized: current node (hxtw8l9lft21rn21x5kmfm06j) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-2jxt8lffza085i7cszgczf0f4tq1l58eshx2pfggglo2nsr5s9-866whibar7pix29ms8blf7hp1 10.200.243.218:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```
- Apaguem el swarm iniciat en aquest exercici.
```
$ docker swarm leave --force
Node left the swarm.
```
## Docker node
### under construction
- Estat del node
    - Pause: Pausa el node.
    - Drain: Finalitza tots els serveis que estiguin associats al node.
    - Active: Reactiva el node.
```
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
pwulz9fu0ikm8wr11zjbzg9iu     i17        Ready     Active                          24.0.2
w2ffqerspj76ncdz8z17qhg80 *   i18        Ready     Active         Leader           24.0.2

$ docker node update --availability pause i17
i17
$ docker node update --availability drain i17
i17
$ docker node update --availability active i17
i17

```
- Posar etiquetes a nodes
```
$ docker node update --label-add lloc=local i17
i17
$ docker node inspect i17
[
    {
        "ID": "pwulz9fu0ikm8wr11zjbzg9iu",
        "Version": {
            "Index": 19
        },
        "CreatedAt": "2023-11-30T12:10:30.232043101Z",
        "UpdatedAt": "2023-11-30T12:15:48.895729118Z",
        "Spec": {
            "Labels": {
                "lloc": "local"
            },
            "Role": "worker",
            "Availability": "active"
........
```
- Indicar colocació de serveis

```
  web:
    image: tcasanovas/comptador:latest 
    deploy:
      replicas: 2
      restart_policy:
        condition: no
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
      placement:
          constraints: [node.labels.lloc == local]

```
