# Repte 1 Getting started with Swarm

## 1 Inicialitzar swarm

```
- docker swarm init

Swarm initialized: current node (57k8ww2e2i1ey2ljuasqzfaeg) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3h4sd1o0vz2kqxztxatpj7t1ju0ied3r0k3xzpywgux5oez8ip-1zrt89bk0qetm7jy27hd754h4 10.200.243.218:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

```

## 2 Add nodes to swarm

Per cada un dels workers executem la següent comanda: 
```
- docker swarm join --token SWMTKN-1-3h4sd1o0vz2kqxztxatpj7t1ju0ied3r0k3xzpywgux5oez8ip-1zrt89bk0qetm7jy27hd754h4 10.200.243.218:2377
```
## Visualitzar tokens worker i manager i observar el swarm
```
docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3h4sd1o0vz2kqxztxatpj7t1ju0ied3r0k3xzpywgux5oez8ip-1zrt89bk0qetm7jy27hd754h4 10.200.243.218:2377

docker swarm join-token manager
To add a manager to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3h4sd1o0vz2kqxztxatpj7t1ju0ied3r0k3xzpywgux5oez8ip-bwxfmwtm5m8oobwh4rgwqgrlm 10.200.243.218:2377

 docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
jxmdp0m4cqkkn7d2eyse9q2o3     i16        Ready     Active                          24.0.2
57k8ww2e2i1ey2ljuasqzfaeg *   i18        Ready     Active         Leader           24.0.2
```

## 3 Deploy a service

```
- docker service create --replicas 1 --name helloworld alpine ping docker.com

wavv9v57ebbmib31ezd3qyl9o
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
verify: Service converged 
```
```
docker service ls
ID             NAME         MODE         REPLICAS   IMAGE           PORTS
wavv9v57ebbm   helloworld   replicated   1/1        alpine:latest  
```
```
docker service ps helloworld
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
kiip81k6vyza   helloworld.1   alpine:latest   i18       Running         Running 5 minutes ago  

```

## 4 Inspect the service

Inspeccionem el servei helloworld

```
docker service inspect --pretty helloworld

ID:		wavv9v57ebbmib31ezd3qyl9o
Name:		helloworld
Service Mode:	Replicated
 Replicas:	1
Placement:
UpdateConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		alpine:latest@sha256:eece025e432126ce23f223450a0326fbebde39cdf496a85d8c016293fc851978
 Args:		ping docker.com 
 Init:		false
Resources:
Endpoint Mode:	vip
```

```
docker inspect helloworld

[
    {
        "ID": "wavv9v57ebbmib31ezd3qyl9o",
        "Version": {
            "Index": 16
        },
        "CreatedAt": "2023-11-22T11:42:16.481612887Z",
        "UpdatedAt": "2023-11-22T11:42:16.481612887Z",
        "Spec": {
            "Name": "helloworld",
            "Labels": {},
            "TaskTemplate": {
                "ContainerSpec": {
                    "Image": "alpine:latest@sha256:eece025e432126ce23f223450a0326fbebde39cdf496a85d8c016293fc851978",
                    "Args": [
                        "ping",
                        "docker.com"
                    ],
                    "Init": false,
                    "StopGracePeriod": 10000000000,
                    "DNSConfig": {},
                    "Isolation": "default"
                },
                "Resources": {
                    "Limits": {},
                    "Reservations": {}
                },
                "RestartPolicy": {
                    "Condition": "any",
                    "Delay": 5000000000,
                    "MaxAttempts": 0
                },
                "Placement": {
                    "Platforms": [
                        {
                            "Architecture": "amd64",
                            "OS": "linux"
                        },
                        {
                            "OS": "linux"
                        },
                        {
                            "OS": "linux"
                        },
                        {
                            "Architecture": "arm64",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "386",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "ppc64le",
                            "OS": "linux"
                        },
                        {
                            "Architecture": "s390x",
                            "OS": "linux"
                        }
                    ]
                },
                "ForceUpdate": 0,
                "Runtime": "container"
            },
            "Mode": {
                "Replicated": {
                    "Replicas": 1
                }
            },
            "UpdateConfig": {
                "Parallelism": 1,
                "FailureAction": "pause",
                "Monitor": 5000000000,
                "MaxFailureRatio": 0,
                "Order": "stop-first"
            },
            "RollbackConfig": {
                "Parallelism": 1,
                "FailureAction": "pause",
                "Monitor": 5000000000,
                "MaxFailureRatio": 0,
                "Order": "stop-first"
            },
            "EndpointSpec": {
                "Mode": "vip"
            }
        },
        "Endpoint": {
            "Spec": {}
        }
    }
]
```
```
docker service ps helloworld
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
kiip81k6vyza   helloworld.1   alpine:latest   i18       Running         Running 8 minutes ago    
```
En el node on s'executi el servei
```
docker ps
CONTAINER ID   IMAGE           COMMAND             CREATED          STATUS          PORTS     NAMES
f7e49f89d612   alpine:latest   "ping docker.com"   10 minutes ago   Up 10 minutes             helloworld.1.kiip81k6vyza94l5waj5xeqx9
```

## 5 Change the scale

Desplegar 5 serveis

```
docker service scale helloworld=5
helloworld scaled to 5
overall progress: 5 out of 5 tasks 
1/5: running   [==================================================>] 
2/5: running   [==================================================>] 
3/5: running   [==================================================>] 
4/5: running   [==================================================>] 
5/5: running   [==================================================>] 
verify: Service converged 
```
```
docker service ps helloworld
ID             NAME           IMAGE           NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
kiip81k6vyza   helloworld.1   alpine:latest   i18       Running         Running 16 minutes ago             
o7y8v1tg520s   helloworld.2   alpine:latest   i16       Running         Running 4 minutes ago              
w1op1tdm00d3   helloworld.3   alpine:latest   i16       Running         Running 4 minutes ago              
vg5jqjbxq72o   helloworld.4   alpine:latest   i18       Running         Running 4 minutes ago              
i869muetflsz   helloworld.5   alpine:latest   i16       Running         Running 4 minutes ago           
```

Des del MANAGER
```
docker ps
CONTAINER ID   IMAGE           COMMAND             CREATED              STATUS              PORTS     NAMES
4308e8910dc7   alpine:latest   "ping docker.com"   About a minute ago   Up About a minute             helloworld.4.vg5jqjbxq72olbg4ut8mk6se5
f7e49f89d612   alpine:latest   "ping docker.com"   14 minutes ago       Up 14 minutes                 helloworld.1.kiip81k6vyza94l5waj5xeqx9
```
Des del WORKER

```
docker ps
CONTAINER ID   IMAGE       	COMMAND         	CREATED     	STATUS     	PORTS 	NAMES
cc256954640e   alpine:latest   "ping docker.com"   3 minutes ago   Up 3 minutes         	helloworld.2.o7y8v1tg520szl1p0tzrwf862
1e6c33233a11   alpine:latest   "ping docker.com"   3 minutes ago   Up 3 minutes         	helloworld.3.w1op1tdm00d3eh9zx7eb9rjmb
b8f4c705113c   alpine:latest   "ping docker.com"   3 minutes ago   Up 3 minutes         	helloworld.5.i869muetflszcvgy7nub1eu3e
```

## 6 Delete the service

```
docker service rm helloworld
helloworld
```
```
docker service ls
ID        NAME      MODE      REPLICAS   IMAGE     PORTS
```
```
docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## 7 Rolling updates

En el manager

```
docker service create \
--replicas 3 \
--name redis \
--update-delay 10s \
redis:3.0.6

wjfdiwlzdrp7pvnu95nadjs5j
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 
```
```
docker service ps redis
ID             NAME      IMAGE         NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
vr9r2xhk0u4k   redis.1   redis:3.0.6   i18       Running         Running about a minute ago             
o85qk7x8wuw0   redis.2   redis:3.0.6   i16       Running         Running about a minute ago             
txchyxets6k9   redis.3   redis:3.0.6   i18       Running         Running about a minute ago             

```
```
docker service inspect --pretty redis

ID:		wjfdiwlzdrp7pvnu95nadjs5j
Name:		redis
Service Mode:	Replicated
 Replicas:	3
Placement:
UpdateConfig:
 Parallelism:	1
 Delay:		10s
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		redis:3.0.6@sha256:6a692a76c2081888b589e26e6ec835743119fe453d67ecf03df7de5b73d69842
 Init:		false
Resources:
Endpoint Mode:	vip

```
```
docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED              STATUS              PORTS      NAMES
c3c8a170cfdb   redis:3.0.6   "/entrypoint.sh redi…"   About a minute ago   Up About a minute   6379/tcp   redis.1.vr9r2xhk0u4k6s5do8er74f5f
7b9c1af62db5   redis:3.0.6   "/entrypoint.sh redi…"   About a minute ago   Up About a minute   6379/tcp   redis.3.txchyxets6k9gywnxi6n9bjoi

```

```
docker service update --image redis:3.0.7 redis
redis
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 

```

```
docker service inspect --pretty redis

ID:		wjfdiwlzdrp7pvnu95nadjs5j
Name:		redis
Service Mode:	Replicated
 Replicas:	3
UpdateStatus:
 State:		completed
 Started:	About a minute ago
 Completed:	28 seconds ago
 Message:	update completed
Placement:
UpdateConfig:
 Parallelism:	1
 Delay:		10s
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		redis:3.0.7@sha256:730b765df9fe96af414da64a2b67f3a5f70b8fd13a31e5096fee4807ed802e20
 Init:		false
Resources:
Endpoint Mode:	vip
```

```
docker service ps redis
ID             NAME          IMAGE         NODE      DESIRED STATE   CURRENT STATE                 ERROR     PORTS
c6py5jjfcz5o   redis.1       redis:3.0.7   i18       Running         Running about a minute ago              
vr9r2xhk0u4k    \_ redis.1   redis:3.0.6   i18       Shutdown        Shutdown about a minute ago             
uren6m14sd8h   redis.2       redis:3.0.7   i16       Running         Running 2 minutes ago                   
o85qk7x8wuw0    \_ redis.2   redis:3.0.6   i16       Shutdown        Shutdown 2 minutes ago                  
qzsup9zp3yp6   redis.3       redis:3.0.7   i18       Running         Running 2 minutes ago                   
txchyxets6k9    \_ redis.3   redis:3.0.6   i18       Shutdown        Shutdown 2 minutes ago  
```
```
docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED         STATUS         PORTS      NAMES
418aec7d344c   redis:3.0.7   "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   6379/tcp   redis.1.c6py5jjfcz5oiqg544h2ochc6
36f462ef0c51   redis:3.0.7   "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   6379/tcp   redis.3.qzsup9zp3yp6n9osyzmme3nfq
```
```
docker service rm redis
redis
```
Si en fer l'actualització queda en pause podem fer la següent ordre:
```
docker service update redis
```
## 8 Drain / pause a node

```
 docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
jxmdp0m4cqkkn7d2eyse9q2o3     i16        Ready     Active                          24.0.2
57k8ww2e2i1ey2ljuasqzfaeg *   i18        Ready     Active         Leader           24.0.2
```

```
docker service create --replicas 3 --name redis --update-delay 10s redis:3.0.6
vdsuxgr5isz3a0z188brb6xxv
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 
```
```
docker service ps redis
ID             NAME      IMAGE         NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
7n1zour9jzbj   redis.1   redis:3.0.6   i18       Running         Running 50 seconds ago             
u495kcez1bbk   redis.2   redis:3.0.6   i16       Running         Running 50 seconds ago             
qkmqx7yrfy0m   redis.3   redis:3.0.6   i16       Running         Running 50 seconds ago             
```

```
docker node update --availability drain tnuge5c69dh17vtye9gya8t77
tnuge5c69dh17vtye9gya8t77
```

```
docker node inspect --pretty tnuge5c69dh17vtye9gya8t77
ID:			tnuge5c69dh17vtye9gya8t77
Hostname:              	i16
Joined at:             	2023-11-22 12:23:26.121397798 +0000 utc
Status:
 State:			Ready
 Availability:         	Drain
 Address:		10.200.243.216
Platform:
 Operating System:	linux
 Architecture:		x86_64
Resources:
 CPUs:			8
 Memory:		15.49GiB
Plugins:
 Log:		awslogs, fluentd, gcplogs, gelf, journald, json-file, local, logentries, splunk, syslog
 Network:		bridge, host, ipvlan, macvlan, null, overlay
 Volume:		local
Engine Version:		24.0.2
TLS Info:
 TrustRoot:
-----BEGIN CERTIFICATE-----
MIIBajCCARCgAwIBAgIUPvwlk3qRjK0GLzXIXGboq9QUKZ4wCgYIKoZIzj0EAwIw
EzERMA8GA1UEAxMIc3dhcm0tY2EwHhcNMjMxMTIyMTEyNTAwWhcNNDMxMTE3MTEy
NTAwWjATMREwDwYDVQQDEwhzd2FybS1jYTBZMBMGByqGSM49AgEGCCqGSM49AwEH
A0IABDOQHXQVu2pmDnpgDV91i7Na8W01ashdbao5jLyjv0LvsUoTJgMjC+GVNv28
gDlV/fyCEsnPIXe3I6xG65K8NHejQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMB
Af8EBTADAQH/MB0GA1UdDgQWBBTg4ACQPQgsJfs+asYBz1WdppYA7zAKBggqhkjO
PQQDAgNIADBFAiBCfM01qUiYKM/vySpxDPToVwY+r/PDy0KUnuEywCzjhAIhAN0y
M8QFcbi8+4RsZk/oEsN85Jp5hLZk/W26vZRZGUNK
-----END CERTIFICATE-----

 Issuer Subject:	MBMxETAPBgNVBAMTCHN3YXJtLWNh
 Issuer Public Key:	MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEM5AddBW7amYOemANX3WLs1rxbTVqyF1tqjmMvKO/Qu+xShMmAyML4ZU2/byAOVX9/IISyc8hd7cjrEbrkrw0dw==

```

```
docker service ps redis
ID             NAME          IMAGE         NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
7n1zour9jzbj   redis.1       redis:3.0.6   i18       Running         Running 7 minutes ago             
izli7y5p34a7   redis.2       redis:3.0.6   i18       Running         Running 4 minutes ago             
u495kcez1bbk    \_ redis.2   redis:3.0.6   i16       Shutdown        Running 7 minutes ago             
tlck9r7feo67   redis.3       redis:3.0.6   i18       Running         Running 4 minutes ago             
qkmqx7yrfy0m    \_ redis.3   redis:3.0.6   i16       Shutdown        Running 7 minutes ago             
```
```
docker node ls

ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
tnuge5c69dh17vtye9gya8t77     i16        Ready     Drain                           24.0.2
57k8ww2e2i1ey2ljuasqzfaeg *   i18        Ready     Active         Leader           24.0.2
```
Podem posar un node que estava active a estat drain, això fa que no accepti tasques i que se li eliminin les que executava passant-les a un altre node. Atenció, es tracta de tasques del swarm, el host pot executar tranquil·lament containers amb docker run i amb docker-compose.

```
docker node update --availability active tnuge5c69dh17vtye9gya8t77
tnuge5c69dh17vtye9gya8t77
```
```
docker node inspect --pretty tnuge5c69dh17vtye9gya8t77
```
```
docker service ps redis
ID             NAME          IMAGE         NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
7n1zour9jzbj   redis.1       redis:3.0.6   i18       Running         Running 11 minutes ago             
izli7y5p34a7   redis.2       redis:3.0.6   i18       Running         Running 8 minutes ago              
u495kcez1bbk    \_ redis.2   redis:3.0.6   i16       Shutdown        Running 11 minutes ago             
tlck9r7feo67   redis.3       redis:3.0.6   i18       Running         Running 8 minutes ago              
qkmqx7yrfy0m    \_ redis.3   redis:3.0.6   i16       Shutdown        Running 11 minutes ago             
```
```
docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
jxmdp0m4cqkkn7d2eyse9q2o3     i16        Down      Active                          24.0.2
tnuge5c69dh17vtye9gya8t77     i16        Ready     Active                          24.0.2
57k8ww2e2i1ey2ljuasqzfaeg *   i18        Ready     Active         Leader           24.0.2
```
```
docker service rm redis
redis
```
