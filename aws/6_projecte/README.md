# Repte 6 AWS Crear màquina virtual Windows

## 6.1 Crear una instància Windows

- Creem una nova instància amb una AMI Windows Server que ens proporciona AWS
- Introduïm el keypair vockey
- Creem un nou Security Groups i sobretot marquem l'opció **Allow RDP traffic from** 
- La mida del disc es de 30gb en comptes de 8gb com a linux.

### 6.1.1 Accedir a la instància Windows usant RPD

- Seleccionem l'instància i cliquem al botó de la part superior **Connect**
- Clic a la part superior **RPD client**
- Ens baixem l'arxiu desktop file fent clic al botó **Download remote desktop file**
- Per obtenir la password per accedir a l'instància cliquem a **get password**
- Seguidament ens demanarà pujar la **keypair vockey** per poder desxifrar la password
- Una vegada desxifrada, executem **remote desktop file** que ens hem baixat anteriorment, tenen extensió **.rdp**
- Acceptem certificats (perquè solen estar desactualitzats)
- Ens demanarà usuari i password




