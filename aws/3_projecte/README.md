# 3) Generar servidor web amb Apache

## 3.1 Crear servidor web amb apache

### Ens connectem a la MV

```
$ ssh -i .ssh/vockey.pem ec2-user@3.84.126.127
```
### Instal·lem servidor web

```
$ sudo yum install httpd
```
###  Editem fitxer index.html (posem el que volguem).
```
[ec2-user@ip-172-31-23-78 html]$ cat index.html 
visca portugal!
l'autonomia que ens cal és la de portugal :D
```
###  Habilitem servei httpd perquè s'inici al iniciar-se el container
```
sudo systemctl enable httpd
sudo systemctl start httpd
sudo systemctl status httpd
```
## 3.2 Gestió Security Groups

### Modifiquem security groups

> - A la pestanya de **instances** seleccionem el nostre servidor web 
> - Anem al apartat de abaix a la pestanya **security** i cliquem al enllaç marcat en blau del nom del nostre security group (si no també podem clicar al apartat de l'esquerra de **Security Groups** i buscar el SG en qüestió.
> - Una vegada dins del security group pertinent, fem clic a **edit inbound rules**.
> - Creem una nova rule HTTP, protocol TCP, port 80, 0.0.0.0 i li posem de nom servidor web per exemple

### Comprovació
> - Dins del container podem utilitzar la següent orde i després fer un cat de index.htl
- wget 3.84.126.127
- cat index.html
> - Des del browser
- 3.84.126.127 al navegador i hauriem de visualitzar la pàgina web

## 3.3 Afegir Docker a la MV i desplegar servei

### Tipus sistema operatiu
```
[ec2-user@ip-172-31-23-78 html]$ uname -a
Linux ip-172-31-23-78.ec2.internal 6.1.61-85.141.amzn2023.x86_64 #1 SMP PREEMPT_DYNAMIC Wed Nov  8 00:39:18 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
```
```
[ec2-user@ip-172-31-23-78 html]$ cat /etc/os-release 
NAME="Amazon Linux"
VERSION="2023"
ID="amzn"
ID_LIKE="fedora"
VERSION_ID="2023"
PLATFORM_ID="platform:al2023"
PRETTY_NAME="Amazon Linux 2023"
ANSI_COLOR="0;33"
CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2023"
HOME_URL="https://aws.amazon.com/linux/"
BUG_REPORT_URL="https://github.com/amazonlinux/amazon-linux-2023"
SUPPORT_END="2028-03-15"
```
```
[ec2-user@ip-172-31-23-78 html]$ yum repolist
repo id                                                 repo name
amazonlinux                                             Amazon Linux 2023 repository
kernel-livepatch                                        Amazon Linux 2023 Kernel Livepatch repository
```

### Instal·lem docker
```
[ec2-user@ip-172-31-23-78 html]$ sudo yum install docker
[ec2-user@ip-172-31-23-78 html]$ sudo systemctl start docker
[ec2-user@ip-172-31-23-78 html]$ systemctl status docker
[ec2-user@ip-172-31-23-78 html]$ sudo usermod -aG docker ec2-user
[ec2-user@ip-172-31-23-78 html]$ exit
logout
```
- Ens connectem una altre vegada
```
$ ssh -i .ssh/vockey.pem ec2-user@3.84.126.127
```
```
[ec2-user@ip-172-31-23-78 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

[ec2-user@ip-172-31-23-78 ~]$ docker network create mynet
da1a58886dd36be6329438d48095a4bf1c2398f0e0c38c2eca379a577e4b17da

[ec2-user@ip-172-31-23-78 ~]$ docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 19:19 -d tcasanovas/net22:base

[ec2-user@ip-172-31-23-78 ~]$ nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 12:12 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00031s latency).
Not shown: 995 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
22/tcp open  ssh
80/tcp open  http
```
```
[ec2-user@ip-172-31-23-78 ~]$ docker network inspect mynet

[ec2-user@ip-172-31-23-78 ~]$ nmap 172.18.0.2
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 12:14 UTC
Nmap scan report for ip-172-18-0-2.ec2.internal (172.18.0.2)
Host is up (0.00023s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen

Nmap done: 1 IP address (1 host up) scanned in 0.08 seconds
```
### Verificar des d'un client de l'aula

```
telnet 3.84.126.127 13
```
### Obrir els ports 7,13,19 al security groups

Seleccionem el security group assignat a la nostre MV
Creem 3 noves regles:
- Custom TCP > port 7 > 0.0.0.0 > echo
- Custom TCP > port 13 > 0.0.0.0 > daytime
- Custom TCP > port 19 > 0.0.0.0 > chargen 

### Assignem nou security group

Anem al apartat instances, seleccionem la instància del servidor web.
Actions > security Groups > change security group > seleccionem security group nou

### Comprovació
```
root@b723a2538ce1:/# telnet 3.84.126.127 13
Trying 3.84.126.127...
Connected to 3.84.126.127.
Escape character is '^]'.
04 DEC 2023 12:26:17 UTC
Connection closed by foreign host.

root@b723a2538ce1:/# telnet 3.84.126.127 7 
Trying 3.84.126.127...
Connected to 3.84.126.127.
Escape character is '^]'.
asdf
asdf
asdf
asdf
asdf
asdf
^]
telnet> 
Connection closed.

root@b723a2538ce1:/# telnet 3.84.126.127 19 | head -n10
Trying 3.84.126.127...
Connected to 3.84.126.127.
Escape character is '^]'.
!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefgh
"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghi
#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghij
$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijk
%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijkl
&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklm
'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmn
```


