# 4 Crear imatge AMI personalitzada

## 4.1 Crear una imatge AMI amb una seu web

- En el apartat de instances, clic dret sobre l'instància desitjada, **image and templates** i després **create image**
- Posem nom, descripció i tot seguit **create image**

### 4.1.1 Gestionar AMIs

- En l'apartat de **AMIs** a l'esquerra, ens apareixeràn totes les AMI que tenim creades.
- Podem:
    - Copiar
    - Desregistar-les
    - Modificar permissos.
    - Afegir/Editar etiquetes.
    - etc.
## 4.2 Crear una AMI personalitzada (compose / passwd / etc)

- Agafem la instància myweb, i creem una nova AMI a partir d'aquella instància.
- Sobretot elegim el **Security Group** corresponent i keypair **vockey**
- Ens connectem per ssh 
```
$ ssh -i .ssh/vockey.pem ec2-user@54.174.181.156
```
- Una vegada dins de la meva nova instància, anomenada **myAMI** farem els següents passos:
```
$ sudo passwd
jupiter
jupiter

$sudo passwd ec2-user
jupiter
jupiter
```
```
$ sudo yum install git nmap tree
$ git clone https://www.gitlab.com/edtasixm05/aws
$ git clone https://www.gitlab.com/edtasixm05/docker
```
### 4.2.1 Instal·lar docker-compose
```
$ sudo curl -SL https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
$ chmod +x /usr/local/bin/docker-compose
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
$ docker-compose ls
NAME STATUS CONFIG FILES

$ docker run --name net.edt.org -h net.edt.org --net mynet --restart unless-stopped -p 7:7 -p 13:13 -p 19:19 -d edtasixm05/net21:base
$ docker ps
```

## 4.2.2 Generar nova AMI sobre aquesta instància creada anteriorment

- Seguim els mateixos passos descrits al principi del document, clic dret sobre l'instància nova > create image ...

## Comprovacions

- Ens connectem
```
a222497tm@i18:~$ ssh -i .ssh/vockey.pem ec2-user@54.174.181.156
```
- Comprovem que el container esta iniciat
```
[ec2-user@ip-172-31-28-140 ~]$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED      STATUS                  PORTS                                                                                                 NAMES
eeea99dec7f1   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   2 days ago   Up Less than a second   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp   net.edt.org

```
- Fem un nmap per comprovar els ports oberts
```
[ec2-user@ip-172-31-28-140 ~]$ nmap localhost

Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-13 12:24 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00033s latency).
Not shown: 995 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
22/tcp open  ssh
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 0.08 seconds

```
- Comprovem el servei daytime (podem comprovar altres serveis, només hem de canviar el número de port
```
[ec2-user@ip-172-31-28-140 ~]$ telnet localhost 13

Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
13 DEC 2023 12:26:43 UTC
Connection closed by foreign host.
```
- Comprovem la nostre pàgina web

```
[ec2-user@ip-172-31-28-140 ~]$ wget 54.174.181.156

--2023-12-13 12:27:46--  http://54.174.181.156/
Connecting to 54.174.181.156:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 62 [text/html]
Saving to: ‘index.html.1’

index.html.1                       100%[================================================================>]      62  --.-KB/s    in 0s      

2023-12-13 12:27:46 (8.33 MB/s) - ‘index.html.1’ saved [62/62]

[ec2-user@ip-172-31-28-140 ~]$ cat index.html.1 

visca portugal!
l'autonomia que ens cal és la de portugal :D

```

