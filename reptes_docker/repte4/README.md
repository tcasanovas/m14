# LDAP amb persistència de dades


## Introducció
```
Primer de tot tenir en compte que al instal·lar ldap (ldap-utils i slapd) la instal·lació
ens obliga a introduïr una contrasenya, això fa que a l'hora de crear-se el container 
es quedi "encatllat" en aquell pas, per tant hem de fer la instal·lació no interactiva,
per això en el Dockerfile posem el argument DEBIAN_FRONTEND=noninteractive

Assignem directori de treball i apart que copi tot el contingut de /opt/docker
i poguem utilitzar els fitxers que estiguin dins d'aquest directori al container.

Obrim el port 389 que és el del servei LDAP.

Haurem de crear 2 volums:
    - 1 Per la configuració del ldap
    - 2 Per les dades del ldap

La configuració de ldap està a: /etc/ldap/slapd.d/
Les dades de ldap estan: /var/lib/ldap/

```
## Creació directori de configuració dinàmica
```
- slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
```
## Injectem la configuració i les dades
```
Configuració (slapd.d) i les dades que volem que tingui la nostre BD de ldap (edt-org.ldif)

- slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
```
## Assignar grups, user corresponents
```
Quan s'instal·la ldap, les BD són propietat de root, hem de canviar-ho a openldap

- chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
```
## Engegar el dimoni de ldap en detach (segon pla)

```
- /usr/sbin/slapd -d0
```
## Engegar container amb volums i persistència de dades

```
- docker run --rm --name ldap.edt.org -h ldap.edt.org -v ldap_config:/etc/ldap/slapd.d -v ldap_dades:/var/lib/ldap tcasanovas/repte4:base
```




