# Repte 3

## Creació container postgres
```
Comanda per un container postgres amb una base de dades buida. S’utilitza una imatge
ja preparada que incorpora postgres.

- docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres

El problema es que aqui no estem utilitzant el nostre propi volum, per tant 
l'hem de crear perquè se'ns guardin les dades.
```
## Creació volum
```
Crearem el volum a on es guardaran les dades del nostre container amb la següent ordre:
- docker volume create <nom_volum>
En el meu cas es el següent:
- docker volume create postgres_info
```
## Creem container amb volum
```
L'argument que hem d'afegir es el -v junt amb el nom del nostre volum (en el meu cas 
postgres_info) i amb el directori /var/lib/postgresql/data

- docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -v postgres_info:/var/lib/postgresql/data -d postgres

```
## Entrem al container
```
Comanda per entrar al container i fer els proves pertinents dins de la BDD training

- docker exec -it postgres psql -U postgres

Finalment executem l'script que en el meu cas està a /docker-entrypoint-initdb.d/
- Per executar l'script dins de postgres hem d'executar \i i el path a on tenim l'script.

```

Com que estem utilitzant un volum, tota la informació del container serà guardada.
 

 

