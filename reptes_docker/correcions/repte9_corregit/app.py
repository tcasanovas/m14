import time
import redis
from flask import Flask

#variable que utilitza un constructor anomenat Flask...
app = Flask(__name__)
cache = redis.Redis(host='redis',port=6379)
# Funció comptador de visites
def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

# La ruta de l'aplicació és a partir de l'arrel.
@app.route('/')
# Funció que mostra 
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
