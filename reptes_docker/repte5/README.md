# Repte 5

## Dockerfile ENTRYPOINT

```
- El Dockerfile serà igual que al repte 4 però amb un alteració, en comptes de CMD hem de posar
ENTRYPOINT:
- ENTRYPOINT ["/bin/bash", "/opt/docker/startup.sh" ] --> aquesta comanda el que fa es executar una shell amb les comandes
que introduïm dins del fitxer startup.sh.

```

## Explicació Startup.sh
```
1. Inicialitza tot de nou i fa populate de edt.org

        rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0;;


    - És el mateix que al repte 4, borrem directoris configuració i dades de ldap.
    - Creem directori dinàmic de configuració
    - Fem injecció de dades a baix nivell
    - Canviem propietari de root a openldap de la bd.
    - Encenem el dimoni en segon pla.

2. Ho inicialitza tot pero sense dades

    - Les mateixes comandes de abans però sense fer la injecció (slapadd).  

3. Engega server utilitzant persistència de dades i que perdurin les execucions ja existents.

    - Creem 2 volums per les dades de ldap i per la configuració.
        - docker volume create ldap_config --> /etc/ldap/slapd.d --> repositori de la config de ldap
        - docker volume create ldap_dades --> /var/lib/ldap --> repositori de les dades de ldap

    - Encenem container amb la següent ordre:

        - docker run --rm --name repte5 -h repte5.edt.org -v ldap_config:/etc/ldap/slapd.d -v ldap_dades:/var/lib/ldap tcasanovas/repte5:base

    - Sobretot comentar o esborrar les 2 primeres línies (rm -rf) perquè així és mantingui les dades i la configuració.

4. Fa slapcat de la base de dades seleccionada

    - Al startup.sh les ordres són les mateixes que en les altres opcions però dins d'un case, depenent del argument que ens passin
    (sigui buit, 0 o 1) ens fara un slapcat de la bd en qüestió.

```
## Encenem el container

```
 - docker run --rm --name repte5 -h repte5.edt.org tcasanovas/repte5:base <opcions>
     (initdb/slapd/start/edtorg/res/slapcat:0,1, o ENTER)
