#! /bin/bash


if [ "$#" == 0 ]; then
	condicio="res"
else
	condicio=$1
fi

arg2=$2
case $condicio in
	# inicialitza tot de nou i fa el populate de edt.org
	initdb)

		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0;;

	# ho inicialitza tot pero nomes engega el servidor, sense posar-hi dades
	slapd)
	
		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0;;

	# engega server utilitzant persistencia de dades de la bd i la config. Engega el sv utilitzant dades ja existents.
	
	start|edtorg|res)

		#rm -rf /etc/ldap/slapd.d/*
		#rm -rf /var/lib/ldap/*
		#slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		#slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
		#chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0;;

	# fa un slapcat de la bse de dades indicada
	
	"slapcat")
		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

		if [ $arg2 == "0" ] || [ $arg2 == "1" ]; then
			slapcat -n$arg2
		elif [ -z "$arg2" ]; then
			slapcat
		fi
		;;
	*)
		echo "Les ordres disponibles estàn dins de: $0"
		exit 1
		;;
		
		

	
esac






