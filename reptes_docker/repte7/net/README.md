# Repte 7

## echo,chargen,daytime

```
Copiar els arxius chargen, daytime i echo que són serveis interns del xinetd.
- Aquests arxius els podem trobar dins del directori: /etc/xinetd.d/

En el meu cas ho he fet al startup.sh
```
## FTP i TFTP
```
Afegir al startup.sh

- service vsftpd start --> per engegar el servei de ftp

Al fitxer vsftpd.conf (fitxer de configuració) afegim "local_root=/srv/ftp" al final del arxiu perquè es
comparteixi la carpeta dels usuaris.

Hem de descomentar les següents linies:

local_enable=YES
write_enable=YES
local_umask=YES
ftpd_banner=Benvingut al FTP d'en Tomas.

```
## Creació usuaris per el FTP
```
#! /bin/bash

for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
done

```

## Comprovacions
```
Echo,daytime,chargen

Molt important afegir el container a una xarxa, per poder fer comprovacions des de fora el container
- Des de dins del container telnet localhost <num_port>
    - Ex: telnet localhost 13 (ens mostra la hora)

Des de fora del container (creem un altre container client):
    - telnet 172.18.0.2 13

FTP:

- ftp <ip>
    - ftp 172.18.0.2
    - Tot seguit ens demanara usuari i contrasenya (entrem amb unix1 per exemple).

- Per sortir dels serveis echo i chargen hem de fer la següent combinació CTRL + ALT GR + claudator tancar



```


## Encendre container net

> - docker run --rm --name net -h net.edt.org --net 2hisx -p 80:80 tcasanovas/repte7:net



