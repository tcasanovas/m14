#! /bin/bash

# Copiem els arxius echo, chargen i daytime al directori en específic.
cp /opt/docker/daytime /etc/xinetd.d
cp /opt/docker/echo /etc/xinetd.d
cp /opt/docker/chargen /etc/xinetd.d
cp /opt/docker/index.html /var/www/html/index.html


systemctl start apache2
service vsftpd start

# Engeguem servei xinetd
xinetd -dontfork

# Engeguem servei apache en FOREGROUND
apachectl -k start -X

