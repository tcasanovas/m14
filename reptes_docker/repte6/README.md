# Repte 6 LDAP canviant admin de la BD a través de variables d'entorn

## DOCKERFILE

```
FROM debian
LABEL author="Tomàs Casanovas"
LABEL subject="ldap amb variables d'entorn i secrets"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y vim tree procps iproute2 ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/ 
ENV LDAP_ROOTDN="tomas"
ENV LDAP_ROOTPW="tomas"
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389


- Molt important la variable de entorn declarar-la al dockerfile per utilitzar-la en el startup.sh
- Es declara amb les sigles ENV i en el nostre cas volem canviar el nom del administrador de la BD, utilitzarem una variable 
universal LDAP_ROOTDN i la canviarem per tomas.
- Farem el mateix en la password del administrador que la canviarem per tomas també.
```

## startup.sh
```
#! /bin/bash



sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf

		rm -rf /etc/ldap/slapd.d/*
		rm -rf /var/lib/ldap/*
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0


- Aqui la part important es el sed, ens permet modificar,buscar,insertar linies especifiques de l'arxiu que indiquem
mitjançant el nostre script.

- Gràcies a les variables d'entorn podem substituir Manager per el meu nom tomas, que li hem indicat previament al dockerfile
i el mateix procés per la password, que passa de secret a tomas. 

```


## Iniciar contenidor
```
docker run --rm --name repte6 -h repte6.edt.org tcasanovas/repte6:base
```

## Comprovació
```
- Executar la comanda:
    -cat slapd.conf
```
